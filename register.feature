Feature: Register user

Scenario: User successfully registered
Given that the user has a phone number
And the network is available with internet connectivity
When the user registers the phone number
Then he is successfully registered with the app

Scenario: User unable to complete registration
Given that the user has a phone number
And the network is unavailable or without internet connectivity
When the user registers the phone number
Then the app should display an error message



